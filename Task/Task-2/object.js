let user = {
    name: "Devang",
    address: {
      personal: {
        line1: "101",
        line2: "street Line",
        city: "NY",
        state: "WX",
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
      office: {
        city: "City",
        state: "Ahmedabad",
        area: {
          landmark: "landmark",
        },
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
    },
    contact: {
      phone: {
        home: "xxx",
        office: "yyy"
      },
      other: {
        fax: '234',
        pin:360301
      },
      email: {
        home: "xxx",
        office: "yyy",
      },
    },
  };
  
let res = {}
function nFlattenObj(user, parent){
    for(let nKey in user){
        let nPropname = parent + '_' + nKey
        if(typeof user[nKey] == 'object'){
            nFlattenObj(user[nKey],nPropname,res);
        }
        else{
            res[nPropname] = user[nKey];
        }
    }
    return res;        
}

console.log(nFlattenObj(user,"user"))
